FROM openjdk:8-jdk
MAINTAINER <n3integration@gmail.com>

EXPOSE 9080

VOLUME /data

COPY target/my-awesome-calculator-*.jar /app/service.jar

CMD ["java", "-jar", "/app/service.jar"]