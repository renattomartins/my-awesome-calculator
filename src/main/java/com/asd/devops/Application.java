package com.asd.devops;

import com.asd.devops.calc.Calculator;

import static spark.Spark.*;

public class Application {

    public static void main(String[] args) {
        port(getHerokuAssignedPort());
        get("/", (req, res) -> {
            res.status(200);
            res.type("text/html");
            return buildHTML();
        });
    }

    static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }

    static String buildHTML() {
        String html = "<html>\n" +
                "<head>\n" +
                "    <title>My Awesome Calculator</title>\n" +
                "    <style>* {font-family: monospace; color: #00FF00; background: black;} </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <h1>My Awesome Calculator</h1>\n" +
                "    <h2>Some operations:</h2>\n" +
                "    <ul>\n" +
                "        <li>Division: 10/2 = " + Calculator.divide(10, 2) + "</li>\n" +
                "        <li>Multiplication: 3*21 = " + Calculator.multiply(3, 21) + "</li>\n" +
                "        <li>Cube root of 27 is: " + Calculator.cubeRoot(27) + "</li>\n" +
                "        <li>Potentiation: 2^3 = " + Calculator.potentiate(2, 3) + "</li>\n" +
                "    </ul>\n" +
                "</body>\n" +
                "</html>\n";

        return html;
    }
}
