package com.asd.devops.calc;

public class Calculator {

	public static double divide (double dividend, double divisor) {
	    return dividend / divisor;
	}

	public static double multiply (double multiplying1, double multiplying2) {
		return multiplying1 * multiplying2;
	}
	
	public static double cubeRoot(double number) {
		return Math.cbrt(number);
	}
	
	public static double potentiate(double base, double exponent) {
		return Math.pow(base, exponent);
	}
}
