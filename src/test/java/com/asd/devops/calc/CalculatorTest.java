package com.asd.devops.calc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

	@Test
	public void shouldReturn2When4by2() {
		assertEquals(2, Calculator.divide(4, 2), 0.001);
	}
	
	@Test
	public void shouldReturn1666When5by3() {
		assertEquals(1.666, Calculator.divide(5, 3), 0.001);
	}

	@Test
	public void shouldReturn4When2times2() {
		assertEquals(4, Calculator.multiply(2, 2), 1);
	}

	@Test
	public void shouldReturn0When4times0() {
		assertEquals(0, Calculator.multiply(4, 0), 1);
	}
	
	@Test
	public void shouldReturn3WhenCubeRootOf27() {
		assertEquals(3, Calculator.cubeRoot(27), 1);
	}
	
	@Test
	public void shouldReturn8When2PotentiatedBy3() {
		assertEquals(8, Calculator.potentiate(2, 3), 1);
	}
}
